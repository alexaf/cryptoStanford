#!/usr/bin/python
# -*- coding: utf-8 -*-

# This script implements a client that receives a file via 
# an insecure channel and a hash via secure channel.  Hash is
# not a hash of the whole file, but it's a hash produced
# server-side, as following :
# 
# Server breaks file in blocks, hashes last block, appends hash
# second-to-last block, and hash again this concatenation. This 
# goes recursively until the very first block of the file.
# Final hash characterizes the whole file and is sent securely
# to client.
#  
# This scheme permits client to authenticate a file while receiving
# it and not wait till whole file is transmitted.
# This is just a proof of concept.

import socket
from Crypto.Hash import SHA256

host = 'localhost'
port = 8080

# This is hash 0, which is initially communicated from
# server to client via a secure channel. Server has produced
# this hash prior to file streaming, using file_authentication.py
hc = '03c08f4ee0b576fe319338139c045c89c3e8e9409633bea29442e21425006ea8'.decode('hex')

def hash_dat(dat):
    h = SHA256.new()
    h.update(dat)
    return h.digest()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))

with open('/tmp/inbound','ab') as file:
    while True:
        data = s.recv(1056)

        if len(data) == 1056:
            if hash_dat(data) == hc:
                file.write(data[:1024])
                hc = data[1024:]
            else:
                print('File Authentication Error')
                break
        elif len(data) < 1056:
            if hash_dat(data) == hc:
                file.write(data)
                print('Successfully received authenticated file')
                break
        else:
            break

s.shutdown(socket.SHUT_WR)
s.close()


