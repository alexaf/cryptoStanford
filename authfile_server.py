#!/usr/bin/python
# -*- coding: utf-8 -*-

# This script implements a server that sends a file via 
# an insecure channel and a hash via secure channel.  Hash is
# not a hash of the whole file, but it's a hash produced
# server-side, as following :
# 
# Server breaks file in blocks, hashes last block, appends hash
# second-to-last block, and hash again this concatenation. This 
# goes recursively until the very first block of the file.
# Final hash characterizes the whole file and is sent securely
# to client.
#  
# This scheme permits client to authenticate a file while receiving
# it and not wait till whole file is transmitted.
# This is just a proof of concept.

from __future__ import division
import socket
import sys
import file_authentication
import math
import os
import time

if len(sys.argv) != 2:
    sys.exit('Usage : ./authfile_server.py <filepath>')

filepath = sys.argv[1]
hash_list = file_authentication.make_hash(filepath)
host= 'localhost'
port = 8080
      
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
s.listen(1)

while True:
    conn , addr = s.accept()
    
    with open(filepath,'rb') as file:
        for i in range (1,len(hash_list)):
            file.seek((i-1) * 1024,0)
            data = file.read(1024) + hash_list[i]
            time.sleep(0.1)
            conn.sendall(data)

        num_blo = int(math.ceil(os.path.getsize(filepath) / 1024))
        file.seek((num_blo - 1) * 1024,0)
        data = file.read()
        time.sleep(0.1)
        conn.sendall(data)
    
    conn.shutdown(socket.SHUT_WR)
    conn.close()
